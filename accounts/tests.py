from django.test import TestCase, Client

# Create your tests here.
from .views import *
from django.apps import apps
from .apps import AccountsConfig

# Testing App
class appTest(TestCase):

	def test_accountsApps(self):
		self.assertEqual(AccountsConfig.name, 'accounts') 
		self.assertEqual(apps.get_app_config('accounts').name, 'accounts')

# Testing URL
class URLTesting(TestCase):
	def test_apakah_url_home_ada(self):
		response = Client().get('/accounts/')
		self.assertEquals(response.status_code,200) 

	def test_apakah_url_login_ada(self):
		response = Client().get('/accounts/login/')
		self.assertEquals(response.status_code,200)

	def test_apakah_url_register_ada(self):
		response = Client().get('/accounts/register/')
		self.assertEquals(response.status_code,200)


class TextTesting(TestCase):

	def test_apakah_home_ada_HomePage(self):
		response = Client().get('/accounts/')
		html_kembalian = response.content.decode('utf8') 
		self.assertIn("HOME PAGE", html_kembalian)

	def test_apakah_login_ada_Login(self):
		response = Client().get('/accounts/login/')
		html_kembalian = response.content.decode('utf8') 
		self.assertIn("LOGIN", html_kembalian)

	def test_apakah_register_ada_Create(self):
		response = Client().get('/accounts/register/')
		html_kembalian = response.content.decode('utf8') 
		self.assertIn("CREATE", html_kembalian)


class TemplateTesting(TestCase):

	def test_apakah_template_home_sesuai(self):
		response = Client().get('/accounts/')
		self.assertTemplateUsed(response, 'index.html')

	def test_apakah_template_login_sesuai(self):
		response = Client().get('/accounts/login/')
		self.assertTemplateUsed(response, 'registration/login.html')


	def test_apakah_template_register_sesuai(self):
		response = Client().get('/accounts/register/')
		self.assertTemplateUsed(response, 'registration/register.html')
